from asist_nlp import NLPManager
from BaseAgent import BootstrapAgent
from BaseAgent.utils import get_metadata_paths



class Observer:

    def __init__(self, context):
        context.redis_bridge.register_callback(self.on_nlp_message, 'nlp')

    def on_nlp_message(self, msg):
        print(f"t={msg.data['time']}: {msg.data['participant_id']} - {msg.data['text']}")
        print(msg.data['codes'])
        print()



def main(data_dir='/data/ASIST/aptima/study-3_2022/'):
    for path in get_metadata_paths(data_dir):
        print('-' * 32, '\n', path, '\n', '-' * 32)

        # Initialize agent
        agent = BootstrapAgent(path)

        # Initialize components
        nlp_manager = NLPManager(agent.context)
        observer = Observer(agent.context)

        # Run agent
        agent.run()



if __name__ == '__main__':
    main()

 