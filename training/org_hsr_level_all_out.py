
import train_nn
import torch
import numpy as np
import pandas as pd
import random
import pickle
import sys
import os

sys.path.append("/home/aishwarya/DialogTag")
from dialog_tag_new import DialogTag
from collections import Counter
from torch.utils.data import DataLoader
from transformers import AdamW, get_linear_schedule_with_warmup
 
from transformers import BertTokenizer, BertModel
import argparse


def get_diag_data(corrections,codes_list,code_ids):
    data_x=[]
    data_y=[]

    
    data_y = [code_ids[cod] for cod in codes_list]
        
    return corrections, data_y

        


def get_data(index,key=None,weights=False):
    (train_transcript_input,train_high_codes_list_input, train_low_codes_list_input,dev_transcript_input, 
     dev_high_codes_list_input, dev_low_codes_list_input
    ) = hsrt_train_dev_data(index)
    
    code_name1=list(set(np.squeeze(train_high_codes_list_input)))
    code_ids1={k:i for i,k in enumerate(code_name1)}
    #code_ids1= {'1': 0, '2': 1, '5': 2, '3': 3, '4': 4, '6': 5}
    #code_ids1 =  {'no_label': 0, 'role': 1, 'markers': 2, 'victim': 3, 'tools': 4, 'rubble': 5}
    
    print("code_ids1",code_ids1)
    print("code_name1",code_name1)
    rev_code_ids1={v:k for k,v in code_ids1.items()}
    
    data_x1, data_y1=get_diag_data(train_transcript_input,train_high_codes_list_input,code_ids1)
    dev_x1, dev_y1=get_diag_data(dev_transcript_input,dev_high_codes_list_input,code_ids1)
    

        
    y_tr1=torch.tensor(data_y1)
    #x_tr1=torch.tensor(data_x1)

    #print("y_tr1",y_tr1)
    y_dev1=torch.tensor(dev_y1)
    #x_dev1=torch.tensor(dev_x1)
    #groundtruth_labels1=diagact_emb.get_groundtruth(dev_codes_list_input_new,code_name1)
    
    #print("y_dev1",y_dev1)
    return data_x1, y_tr1, dev_x1, y_dev1,code_ids1, code_name1
   
    
def get_key_data(index,key=None,weights=False):
    (train_transcript_input,train_high_codes_list_input, train_low_codes_list_input,dev_transcript_input, 
     dev_high_codes_list_input, dev_low_codes_list_input
    ) = hsrt_train_dev_data(index)
    
    valid_codes_dict={'g':'rubble','m':'markers', 'r':'role','t':'tools','v':'victim'}
    #valid_codes_dict={'m':'markers','v':'victim'}
    #valid_codes_dict={'m':'markers'}
    train_low_codes_list_input_new=[valid_codes_dict[c] if c in valid_codes_dict.keys() else 'no_label' for c in  train_low_codes_list_input]
    dev_low_codes_list_input_new=[valid_codes_dict[c] if c in valid_codes_dict.keys() else 'no_label' for c in  dev_low_codes_list_input]
    
    code_name1=list(set(np.squeeze(train_low_codes_list_input_new)))
    code_ids1={k:i for i,k in enumerate(code_name1)}
    print("code_ids1",code_ids1)
    print("code_name1",code_name1)
    rev_code_ids1={v:k for k,v in code_ids1.items()}
    
    data_x1, data_y1=get_diag_data(train_transcript_input,train_low_codes_list_input_new,code_ids1)
    dev_x1, dev_y1=get_diag_data(dev_transcript_input,dev_low_codes_list_input_new,code_ids1)
    

        
    y_tr1=torch.tensor(data_y1)
   
    y_dev1=torch.tensor(dev_y1)
   
    return data_x1, y_tr1, dev_x1, y_dev1,code_ids1, code_name1
   


def filter_files(files):
    new_files=[]
    for input_file in files:
        df = pd.read_csv(input_file)
        if 'Codes' not in df.columns:
            continue
        if 'Corrections' not in df.columns:
            continue
        if 'float' in str(df['Codes'].dtype):
            continue
        new_files.append(input_file)
    return new_files
   
def read_hsr_file(input_file,max_len= 1000):    
    #print("input_file",input_file)
    text_col='Corrections'
    df = pd.read_csv(input_file)
    #print(df.columns)
    df['Codes'] = df['Codes'].fillna('0')
    start_row=2
    high_codes_new=[]
    low_codes_new=[]
    correction_new=[]
    for i in range(start_row,len(df),4):
        c=df['Codes'].iloc[i]#.split(',')
        c=c.lower().strip()
        if df[text_col].iloc[i] is np.nan:
            continue
        corr=df[text_col].iloc[i].split(':')[-1]

        #print("corr",corr)
        if c[0] not in ['1','2','3','4','5','6','7']:
            continue
        if c[0] !='7':
            high_codes_new.append(c[0].strip())
            if len(c)==2 and c[1] in ['g', 'm', 'r', 't', 'v']:
                low_codes_new.append(c[1].strip())
            else:
                low_codes_new.append('no_label')
            corr= corr.lower().strip()

            if max_len is not None:
                corr=corr[:max_len]
            corr=corr.replace('.','')
            corr=corr.replace('?','')
            correction_new.append(corr)
        if c[0] =='7' and len(correction_new)>0:
            corr_new=correction_new.pop(-1)
            corr=df[text_col].iloc[i].split(':')[-1]
            corr= corr.lower().strip()
            corr_new =corr_new+ ' ' +corr
            if max_len is not None:
                corr_new=corr_new[:max_len]
            corr=corr.replace('.','')
            corr=corr.replace('?','')
            correction_new.append(corr_new)

    return correction_new,high_codes_new,low_codes_new


def hsrt_train_dev_data(index):

    cross =6
    path = '/data/aishwarya/HSR_Zoom_Transcripts_3/'
    files = [ os.path.join(path, f) for f in os.listdir(path) if ('.csv' in f) and ('team' in f) and ('trial' in f)]
    files = filter_files(files)
    #files = sorted(files)
    #random.shuffle(files)
   
    files =np.array(files)
    #print("index",index,index[:12],index[-2:])
#     train_files = files[index[:12]]
#     dev_files = files[index[-2:]]
    ind = len(files)//cross
    index_len = list(range(ind *index,ind *(index+1) ))
    dev_files = files[index_len]
    train_files = [f for f in files if f not in dev_files]
    print((train_files),(dev_files))
    

    #train_transcript_input,train_obs_code_input, train_low1_code_input, train_low1_code_input = [], [],[], []
    train_transcript_input,train_high_codes_list_input, train_low_codes_list_input = [], [],[]
    for input_file in train_files:
        #print(input_file)
       
        transcript_in,high_codes_list_in,low_codes_list_in = read_hsr_file(input_file)
        train_transcript_input.extend(transcript_in)
        train_high_codes_list_input.extend(high_codes_list_in)
        train_low_codes_list_input.extend(low_codes_list_in)
        
        
    dev_transcript_input, dev_high_codes_list_input, dev_low_codes_list_input = [], [],[]
    for input_file in dev_files:
      
        transcript_in,high_codes_list_in,low_codes_list_in = read_hsr_file(input_file)
        dev_transcript_input.extend(transcript_in)
        dev_high_codes_list_input.extend(high_codes_list_in)
        dev_low_codes_list_input.extend(low_codes_list_in)
        
    return train_transcript_input,train_high_codes_list_input, train_low_codes_list_input,dev_transcript_input, dev_high_codes_list_input, dev_low_codes_list_input
    


        
def f1(y_hat,y,label):
    tp = sum((y_hat==label) & (y==label))
    fp = sum((y_hat==label) & (y!=label))
    fn = sum((y_hat!=label) & (y==label))
    r = tp/float(tp + fn + 1e-10)
    p = tp/float(tp + fp + 1e-10)
    f = 2 * r * p / (r + p + 1e-10)
    return r, p, f

def macro_f1(y,y_hat):
    all_labels = set(y)
    y_hat = np.array(y_hat)
    y = np.array(y)
    out = {label:f1(y_hat,y,label) for label in all_labels}
    #print("out",out)
    rs=sum([k[0] for k in out.values()])/len(out)
    ps=sum([k[1] for k in out.values()])/len(out)
    fs=sum([k[2] for k in out.values()])/len(out)
    out['avg']=[rs,ps,fs]

    
    return rs, ps, fs, out

def dev_macro_accuracy(y_test,y_pred_tags):
    correct_pred = (y_pred_tags == y_test)
    acc = correct_pred.sum() / len(correct_pred)
    return acc

def dev_accuracy(groundtruth_labels,predicted_labels):
    actual_acc={}
    acc = 0.
    all_labels = set(groundtruth_labels)
    for key in all_labels:
        true=[1 if key in gt else 0 for gt in groundtruth_labels ]
        pred=[1 if (key in pt) and (true[i]==1) else 0 for i,pt in enumerate(predicted_labels) ]
        a = sum(pred)/sum(true)
        actual_acc[key]=a
        
    actual_acc["avg"]=sum([v for v in actual_acc.values()])/len(actual_acc)
    return actual_acc

def macro_accuracy(y_gnd,y_pred):
    all_labels = set(y_gnd)
    y_gnd=np.array(y_gnd)
    y_pred=np.array(y_pred)
    acc={}
    for lab in all_labels:
        ind=[i for i,y in enumerate(y_gnd) if y==lab]
        acc[lab]=dev_accuracy(y_gnd[ind],y_pred[ind])
    
    acc['avg']=sum(acc.values())/len(acc)
    return acc

def build_linear_0_new(input_size=768, target_size=10):
    #change these
#     size1 = -1; raise NotImplementedError
#     size2 = -1; raise NotImplementedError
    size1 = input_size
    size2 = target_size
    size3=128
    #size3=input_size
    model = torch.nn.Sequential()
    #model.add_module("Linear_in",torch.nn.Linear(size1, size2, bias=True))
    
    model.add_module("Linear_in",torch.nn.Linear(size1, size3, bias=True))
    model.add_module("relu", torch.nn.ReLU())
    model.add_module("dropout", torch.nn.Dropout(0.5))
    model.add_module("Linear_out",torch.nn.Linear(size3, size2, bias=True))
    
    #model.add_module('sigmoid',torch.nn.Sigmoid())

    return model

class DiagBertModel(torch.nn.Module):
    def __init__(self, diag, input_size=768, target_size=10):
        super(DiagBertModel, self).__init__()
        bert = diag.return_pymodel()
        #bert =  AutoModel.from_pretrained("./Diagact_maskedLM_model/")
        self.tokenizer=diag.return_tokenizer()
        size1 = input_size
        size2 = target_size
        size3=128
        self.x1 = bert
        self.dropout = torch.nn.Dropout(0.5)
        self.x2 = torch.nn.Linear(size1, size3, bias=True)
        self.relu = torch.nn.ReLU()
        #self.x3 = torch.nn.Linear(size3, size2, bias=True)
        self.x3 = torch.nn.Linear(size1, size2, bias=True)
        
       

    def forward(self, input_ids, attention_mask):
        #with torch.no_grad():
        outputs=self.x1(input_ids, attention_mask=attention_mask)
        last_hidden_state_cls = outputs[0][:, 0, :]
        #print("last_hidden_state_cls",last_hidden_state_cls.size())
        x = last_hidden_state_cls
        #x = self.dropout(x)
        #x = self.x2(x)
#         x = self.relu(x)
#         x = self.dropout(x)
        output = self.x3(x)
        return output
    
class KeyBertModel(torch.nn.Module):
    def __init__(self, input_size=768, target_size=10):
        super(KeyBertModel, self).__init__()
        bert = BertModel.from_pretrained('bert-base-uncased')
        #bert = BertModel.from_pretrained('./BERT_maskedLM_model/')
        self.tokenizer=BertTokenizer.from_pretrained('bert-base-uncased')
        size1 = input_size
        size2 = target_size
        size3=128
        self.x1 = bert
        self.dropout = torch.nn.Dropout(0.5)
        self.x2 = torch.nn.Linear(size1, size3, bias=True)
        self.relu = torch.nn.ReLU()
        #self.x3 = torch.nn.Linear(size3, size2, bias=True)
        self.x3 = torch.nn.Linear(size1, size2, bias=True)
        
       

    def forward(self, input_ids, attention_mask):
        #with torch.no_grad():
        outputs=self.x1(input_ids, attention_mask=attention_mask)
        last_hidden_state_cls = outputs[0][:, 0, :]
        #print("last_hidden_state_cls",last_hidden_state_cls.size())
        x = last_hidden_state_cls
        #x = self.dropout(x)
        #x = self.x2(x)
#         x = self.relu(x)
#         x = self.dropout(x)
        output = self.x3(x)
        return output
    
class DiagBertModel_StopGrad(torch.nn.Module):
    def __init__(self, diag, input_size=768, target_size=10):
        super(DiagBertModel_StopGrad, self).__init__()
        bert = diag.return_pymodel()
        self.tokenizer=diag.return_tokenizer()
        size1 = input_size
        size2 = target_size
        size3=128
        self.x1 = bert
        self.dropout = torch.nn.Dropout(0.5)
        self.x2 = torch.nn.Linear(size1, size3, bias=True)
        self.relu = torch.nn.ReLU()
        #self.x3 = torch.nn.Linear(size3, size2, bias=True)
        self.x3 = torch.nn.Linear(size1, size2, bias=True)
        
       

    def forward(self, input_ids, attention_mask):
        with torch.no_grad():
            outputs=self.x1(input_ids, attention_mask=attention_mask)
            last_hidden_state_cls = outputs[0][:, 0, :]
            #print("last_hidden_state_cls",last_hidden_state_cls.size())
            x = last_hidden_state_cls
            
#         x = self.relu(x)
#         x = self.dropout(x)
        #x = self.x2(x)
#         x = self.relu(x)
#         x = self.dropout(x)
        output = self.x3(x)
        return output

class DiagBertModel_NoStopGrad(torch.nn.Module):
    def __init__(self, diag, input_size=768, target_size=10):
        super(DiagBertModel_NoStopGrad, self).__init__()
        bert = diag.return_pymodel()
        self.tokenizer=diag.return_tokenizer()
        size1 = input_size
        size2 = target_size
        size3=128
        self.x1 = bert
        self.dropout = torch.nn.Dropout(0.5)
        self.x2 = torch.nn.Linear(size1, size3, bias=True)
        self.relu = torch.nn.ReLU()
        #self.x3 = torch.nn.Linear(size3, size2, bias=True)
        self.x3 = torch.nn.Linear(size1, size2, bias=True)
        
       

    def forward(self, input_ids, attention_mask):
        #with torch.no_grad():
        outputs=self.x1(input_ids, attention_mask=attention_mask)
        last_hidden_state_cls = outputs[0][:, 0, :]
        #print("last_hidden_state_cls",last_hidden_state_cls.size())
        x = last_hidden_state_cls
            
#         x = self.relu(x)
#         x = self.dropout(x)
        #x = self.x2(x)
#         x = self.relu(x)
#         x = self.dropout(x)
        output = self.x3(x)
        return output


def run_eval(model,val_loader,code_name,code_ids,device):
    model.eval()
    with torch.no_grad():
        predicted_labels=[]
        groundtruth_labels=[]
        sentences= []
        rev_code_ids={v:k for k,v in code_ids.items()}
        #print("code_ids",code_ids)
        for batch in val_loader:
        #_, Y_hat = model.forward(X_dv_var).max(dim=1)
            input_ids = batch['input_ids'].to(device)
            attention_mask = batch['attention_mask'].to(device)
            labels = batch['labels'].to(device)
            output = model(input_ids, attention_mask=attention_mask)

#             pred_y = torch.sigmoid(output)
#             #print(Y_hat[:5])
#             pred_y =  (pred_y >=0.5).int()
            
            y_pred_softmax = torch.log_softmax(output, dim = 1)
            _, pred_y = torch.max(y_pred_softmax, dim = 1)   


#             predicted_labels.extend(train_nn.yhat_labels(pred_y.cpu().detach().numpy(),code_ids))
#             groundtruth_labels.extend(train_nn.yhat_labels(labels.cpu().detach().numpy(),code_ids))
            
            predicted_labels.extend([rev_code_ids[c] for c in pred_y.cpu().detach().numpy()])
            groundtruth_labels.extend([rev_code_ids[c] for c in labels.cpu().detach().numpy()])
            
#             print("predicted_labels",predicted_labels,pred_y)
#             print("groundtruth_labels",groundtruth_labels,labels)
        #print(predicted_labels[:5])
        
        # save
        
    
    return np.array(predicted_labels), np.array(groundtruth_labels)



def train_model_new_multiclass(model, train_loader,
                num_its = 5,
                val_loader = None,
                status_frequency=10,
                lr=2e-5,
                adam_epsilon = 1e-8,
                batch_size=16,
                param_file = 'best.params',
                num_warmup_steps= 0,
               code_ids= None,
               code_name= None,
                    device='cpu'
               ):

    
    
    optimizer = AdamW(model.parameters(), lr=lr,eps=adam_epsilon,correct_bias=False)  # To reproduce BertAdam specific 
    total_steps = len(train_loader) * num_its
    
    scheduler = get_linear_schedule_with_warmup(optimizer, num_warmup_steps=num_warmup_steps, num_training_steps=total_steps)  # PyTorch scheduler
    
 
    bce_criterion = torch.nn.CrossEntropyLoss()

    losses = []
    accuracies = []
    final_acc=[]
    final_prediction=[]
    for epoch in range(num_its):
        # set gradient to zero
        batch_loss = 0 
        for batch_ind, batch in enumerate(train_loader):
            
            input_ids = batch['input_ids'].to(device)
            attention_mask = batch['attention_mask'].to(device)
            labels = batch['labels'].to(device)
            output = model(input_ids, attention_mask=attention_mask)
            

            model.train()
            optimizer.zero_grad()
            # run model forward to produce loss
            with torch.enable_grad():
                
                loss= bce_criterion(output,labels)
                
                # backpropagate and train
                loss.backward()
                torch.nn.utils.clip_grad_norm_(model.parameters(), 1.0)
                optimizer.step()
                scheduler.step()
                
            batch_loss += loss.item()
            #losses.append(batch_loss/len(train_loader))


            # write parameters if this is the best epoch yet
            if val_loader is not None:
                # run forward on dev data
                
                predicted_labels,groundtruth_labels= run_eval(model,val_loader,code_name,code_ids,device)
#                 print("groundtruth_labels",(groundtruth_labels))
#                 print("predicted_labels",(predicted_labels))
                #print("groundtruth size",np.shape(predicted_labels),np.shape(groundtruth_labels))
                #precision,recall,fscore,final_acc=train_nn.dev_fscore(groundtruth_labels,predicted_labels,code_name)
                #actual_acc = train_nn.dev_accuracy(groundtruth_labels,predicted_labels,code_name)
                
                precision,recall,fscore,final_acc = macro_f1(groundtruth_labels,predicted_labels)
                actual_acc = dev_accuracy(groundtruth_labels,predicted_labels)
                #acc=precision
                #acc=fscore
                acc= actual_acc['avg']
                if len(accuracies) == 0 or acc > max(accuracies):
                    #acc,final_acc_new=train_nn.dev_fscore(groundtruth_labels,predicted_labels,code_name,show=False)
                    final_acc_new = final_acc
                    actual_acc_new = actual_acc
                    state = {'state_dict':model.state_dict(),
                             'epoch':len(accuracies)+1,
                             'accuracy':acc}
                    #print("epoch, acc",epoch,acc)
                    final_prediction=predicted_labels
                    torch.save(state,param_file)
                accuracies.append(acc)


                # print status message if desired
                #if status_frequency > 0 and epoch % status_frequency == 0:
                    #print("Epoch "+str(epoch+1)+": Dev Accuracy: "+str(acc))
                print("Epoch "+str(epoch+1)+" Train loss: "+str(loss.item())+": Dev precision: "+str(precision)
                     +": Dev recall: "+str(recall)+": Dev fscore: "+str(fscore)+": Dev accuracy: "+str(actual_acc))

    # load parameters of best model
    checkpoint = torch.load(param_file)
    model.load_state_dict(checkpoint['state_dict'])
    
    return model, losses, accuracies,final_acc_new,actual_acc_new,final_prediction,groundtruth_labels



class TranscriptDataset(torch.utils.data.Dataset):
    def __init__(self, encodings, labels):
        self.encodings = encodings
        self.labels = labels

    def __getitem__(self, idx):
        item = {key: torch.tensor(val[idx]) for key, val in self.encodings.items()}
        item['labels'] = torch.tensor(self.labels[idx])
        return item

    def __len__(self):
        return len(self.labels)


def sampler_(train_y):
    labels=train_y.cpu().detach().numpy()
    _, counts = np.unique(labels, return_counts=True)
    weights = 1.0 / torch.tensor(counts, dtype=torch.float)
    #print(counts)
    sample_weights = weights[labels]
#     print("sample_weights",[(s,l) for s,l in zip(sample_weights,labels)])
#     print("labels",len(labels),labels)
    sampler = torch.utils.data.sampler.WeightedRandomSampler(sample_weights, len(sample_weights), replacement=True)
    return sampler


    
def run_bert(train_x, train_y, dev_x, dev_y,args):
    #print("batch_size", args.batch_size)
    batch_size = args.batch_size
  
    code_ids= {'1': 0, '2': 1, '5': 2, '3': 3, '4': 4, '6': 5}
    code_name = list(code_ids.keys())
    train_y=torch.tensor(train_y)
    dev_y=torch.tensor(dev_y)
    
    diag=DialogTag('bert-base-uncased')
    tokenizer = diag.return_tokenizer()
   
    train_encodings = tokenizer(train_x, truncation=True, padding=True)
    val_encodings = tokenizer(dev_x, truncation=True, padding=True)

    train_dataset = TranscriptDataset(train_encodings, train_y)
    val_dataset = TranscriptDataset(val_encodings, dev_y)
    train_sampler = sampler_(train_y)

    train_loader = DataLoader(train_dataset, batch_size=batch_size, shuffle=False, sampler=train_sampler)
    val_loader = DataLoader(val_dataset, batch_size=batch_size, shuffle=False)
    device = torch.device(args.device) if torch.cuda.is_available() else torch.device('cpu')

    nn_model1 = DiagBertModel(diag,target_size=len(code_ids))
    nn_model1.to(device)
    
    
    nn_model1, losses1, accuracies1, final_acc1,actual_acc1, predicted_labels1,groundtruth_labels1=train_model_new_multiclass(nn_model1, train_loader,
                    num_its = args.num_its,
                     val_loader=val_loader,
                        batch_size=batch_size,
                   param_file = args.saved_model,
                   code_ids= code_ids,
                   code_name= code_name,
                    device=device,                                                                                                                  lr=args.lr)
                                                                                                                              
                                                                               
                    
    

                
    counter_dict={}
    count_dict={}
    
    
        
    return final_acc1,actual_acc1,predicted_labels1,groundtruth_labels1,counter_dict,count_dict,nn_model1
    




def run_key_bert(train_x, train_y, dev_x, dev_y,args):
    #batch_size = 16
    batch_size = args.batch_size
    #max_length = 128
    
 
    code_ids =  {'no_label': 0, 'role': 1, 'markers': 2, 'victim': 3, 'tools': 4, 'rubble': 5}
    code_name = list(code_ids.keys())
    
    train_y=torch.tensor(train_y)
    dev_y=torch.tensor(dev_y)
    
    tokenizer=BertTokenizer.from_pretrained('bert-base-uncased')

    #train_encodings = tokenizer(train_x, truncation=True, padding=True, max_length=max_length)
    train_encodings = tokenizer(train_x, truncation=True, padding=True)
    val_encodings = tokenizer(dev_x, truncation=True, padding=True)

    train_dataset = TranscriptDataset(train_encodings, train_y)
    val_dataset = TranscriptDataset(val_encodings, dev_y)
    train_sampler = sampler_(train_y)

    train_loader = DataLoader(train_dataset, batch_size=batch_size, shuffle=False, sampler=train_sampler)
    val_loader = DataLoader(val_dataset, batch_size=batch_size, shuffle=False)
    device = torch.device(args.device) if torch.cuda.is_available() else torch.device('cpu')

    nn_model1 = KeyBertModel(target_size=len(code_ids))
    nn_model1.to(device)
    
    
    nn_model1, losses1, accuracies1, final_acc1,actual_acc1, predicted_labels1,groundtruth_labels1=train_model_new_multiclass(nn_model1, train_loader,
                    num_its = args.num_its,
                     val_loader=val_loader,
                        batch_size=batch_size,
                   param_file = args.saved_model ,
                   code_ids= code_ids,
                   code_name= code_name,
                    device=device ,                                                                                                           lr=args.lr )
               
                    
    

                
    counter_dict={}
    count_dict={}
   
    
    
        
    return final_acc1,actual_acc1,predicted_labels1,groundtruth_labels1,counter_dict,count_dict,nn_model1
    

def arguments():
    parser = argparse.ArgumentParser()
    
    parser.add_argument("--batch_size", type=int, default=32, help='batch_size')
    parser.add_argument("--lr", type=float, default=2e-5, help='learning rate')
    parser.add_argument("--codes", type=str, default='high', help='type of codes high/topic')
    parser.add_argument("--saved_model", type=str, default='high_best_1.params', help='Name of model to be saved')
    parser.add_argument("--num_its", type=int, default=5, help='Number of iterations/epoch')
    parser.add_argument("--device", type=str, default='cuda:3', help='GPU name')
    parser.add_argument("--cross", type=int, default=6, help='Number of folds')
    
    args = parser.parse_args()
    return args

def train_model(train_x, train_y, dev_x, dev_y):
    args = arguments()

    all_final_acc=[]
    all_actual_acc=[]
    if args.codes == 'high':
        (final_acc1,actual_acc1,predicted_labels1,groundtruth_labels1,
         counter_dict,count_dict,nn_model)=run_bert(train_x, train_y, dev_x, dev_y,args)
    else:
        (final_acc1,actual_acc1,predicted_labels1,groundtruth_labels1,
             counter_dict,count_dict,nn_model)=run_key_bert(train_x, train_y, dev_x, dev_y,args)
    all_final_acc.append(final_acc1)
    all_actual_acc.append(actual_acc1)
    return all_actual_acc


    

if __name__ == "__main__":   
    args = arguments()
    if args.codes == 'high':
        train_x, train_y, dev_x, dev_y, code_ids1, code_name1= get_data(index=1)
    else:
        train_x, train_y, dev_x, dev_y,code_ids1, code_name1 = get_key_data(index=1)
        
    all_actual_acc = train_model(train_x, train_y, dev_x, dev_y)

    for key in all_actual_acc[0].keys():
        avg_actual_acc[key] = avg_actual_acc[key]/len(all_actual_acc)

    print(avg_final_acc)
    print("Dataframe avg_actual_acc")
    print(pd.DataFrame.from_dict(avg_actual_acc, orient='index').transpose())