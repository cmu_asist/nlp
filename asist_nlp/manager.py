import logging
import nltk

from MinecraftBridge.messages import ASR_Message
from MinecraftBridge.mqtt.interfaces import Scheduler
from MinecraftBridge.utils import Loggable



class NLPManager(Loggable):
    """
    A lightweight wrapper around NLP models that handles:
        1) registering callbacks with a MissionContext
        2) upon each utterance, sending the NLP predictions on the Redis bus
    """

    def __init__(
        self, context,
        level_1_path='/data/aishwarya/final_models/endtoend_v1.params',
        device=None,
        **kwargs):
        """
        Arguments:
            - context: BaseAgent.MissionContext instance
            - level_1_path: path to saved BERT model parameters (for level 1)
            - device: device to use for PyTorch/Tensorflow models
        """
        self.__set_verbosity()

        # Initialize context and register callback
        self.context = context
        self.context.minecraft_interface.register_callback(ASR_Message, self.on_asr_message)

        # Initialize models
        self.logger.info(f"{self}:  Initializing models ...")
        self.__init_models(level_1_path=level_1_path, device=device)

        # Download NLTK resources
        self.logger.info(f"{self}:  Downloading NLTK resources ...")
        ntlk_quiet = (self.logger.getEffectiveLevel() > logging.INFO)
        nltk.download('averaged_perceptron_tagger', quiet=ntlk_quiet)
        nltk.download('omw-1.4', quiet=ntlk_quiet)
        nltk.download('stopwords', quiet=ntlk_quiet)
        nltk.download('wordnet', quiet=ntlk_quiet)

        # Mission timer
        self.scheduler = Scheduler(context.minecraft_interface)


    def __str__(self):
        """
        Return a string representation of the object.
        """
        return f'[{self.__class__.__name__}]'


    def on_asr_message(self, msg):
        """
        Given observed player communication, 
        publish communication codes from NLP models on the internal bus.

        Arguments:
            - msg: MinecraftBridge.messages.ASR_Message
        """
        codes = {
            key: model.predict([msg.text])[0] 
            for key, model in self.models.items()
        }

        data = {
            'utterance_id': msg.id, 'participant_id': msg.participant_id,
            'time': self.scheduler.time, 'text': msg.text, 'codes': codes,
        }

        self.logger.debug(f'{self}:  Sending NLP message for utterance {msg.id}.')
        self.context.redis_interface.send(data, channel='nlp')


    def __set_verbosity(self):
        """
        Set transformers and tensorflow verbosity from the current logging level.
        """
        if self.logger.getEffectiveLevel() > logging.INFO:
            import os
            os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

        if self.logger.getEffectiveLevel() > logging.DEBUG:
            import transformers
            transformers.logging.set_verbosity_error()

            import tensorflow as tf
            tf.get_logger().setLevel('ERROR')


    def __init_models(self, level_1_path=None, device=None):
        """
        Initialize NLP models.
        """
        self.models = {}

        # Level 1
        try:
            from .models import level1_code
            self.models['level_1'] = level1_code.Level1Model(level_1_path, device=device)
        except Exception as e:
            self.logger.error(f"{self}:  Could not load level 1 model - {e}")

        # Level 2
        from .models import level2_code
        self.models['level_2'] = level2_code

