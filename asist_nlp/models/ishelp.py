import torch
import numpy as np

from transformers import BertTokenizer, BertModel


def build_linear_0_new(input_size=768, target_size=10):
    #change these
#     size1 = -1; raise NotImplementedError
#     size2 = -1; raise NotImplementedError
    size1 = input_size
    size2 = target_size
    size3=128
    #size3=input_size
    model = torch.nn.Sequential()
    #model.add_module("Linear_in",torch.nn.Linear(size1, size2, bias=True))
    
    model.add_module("Linear_in",torch.nn.Linear(size1, size3, bias=True))
    model.add_module("relu", torch.nn.ReLU())
    model.add_module("dropout", torch.nn.Dropout(0.5))
    model.add_module("Linear_out",torch.nn.Linear(size3, size2, bias=True))
    
    #model.add_module('sigmoid',torch.nn.Sigmoid())

    return model


    
class KeyBertModel(torch.nn.Module):
    def __init__(self, input_size=768, target_size=10):
        super(KeyBertModel, self).__init__()
        bert = BertModel.from_pretrained('bert-base-uncased')
        self.tokenizer=BertTokenizer.from_pretrained('bert-base-uncased')
        size1 = input_size
        size2 = target_size
        size3=128
        self.x1 = bert
        self.dropout = torch.nn.Dropout(0.5)
        self.x2 = torch.nn.Linear(size1, size3, bias=True)
        self.relu = torch.nn.ReLU()
        #self.x3 = torch.nn.Linear(size3, size2, bias=True)
        self.x3 = torch.nn.Linear(size1, size2, bias=True)
        
       

    def forward(self, input_ids, attention_mask):
        #with torch.no_grad():
        outputs=self.x1(input_ids, attention_mask=attention_mask)
        last_hidden_state_cls = outputs[0][:, 0, :]
        #print("last_hidden_state_cls",last_hidden_state_cls.size())
        x = last_hidden_state_cls
        #x = self.dropout(x)
        #x = self.x2(x)
#         x = self.relu(x)
#         x = self.dropout(x)
        output = self.x3(x)
        return output

class ishelpModel:
    def __init__(self,param_file):
        self.tokenizer = BertTokenizer.from_pretrained('bert-base-uncased')

        self.code_to_label={0: 'not_help', 1: 'is_help'}
        #self.code_to_label = code_to_label
        self.param_file = param_file
        self.bert_model = KeyBertModel(target_size=len(self.code_to_label))
        checkpoint = torch.load(self.param_file)
        self.bert_model.load_state_dict(checkpoint['state_dict'])
        self.device = torch.device('cuda:1') if torch.cuda.is_available() else torch.device('cpu')
        self.bert_model.to(self.device)
        
        
        
    def predict(self,sentence):
        self.bert_model.eval() 
        with torch.no_grad():
            train_encodings = self.tokenizer(sentence, truncation=True, padding=True)
            input_ids = torch.tensor(train_encodings['input_ids']).to(self.device)
            attention_mask = torch.tensor(train_encodings['attention_mask']).to(self.device)
            output = self.bert_model(input_ids, attention_mask=attention_mask)
            y_pred_softmax = torch.log_softmax(output, dim = 1)
            _, pred_y = torch.max(y_pred_softmax, dim = 1) 

            prediction=[self.code_to_label[c] for c in pred_y.cpu().detach().numpy()]
        return prediction

