N=7
CUDA_VISIBLE_DEVICES=$N python3 run_generation.py \
--model_type gpt2 \
--model_name_or_path ~/output \
--length 20 \
--prompt "<BOS>" \
--stop_token "<EOS>" \
--k 50 \
--num_return_sequences 5