from nltk.translate.bleu_score import corpus_bleu

ref_file = r"~/transcripts_test.txt"
can_file = r"~/transcripts_test_gen.txt"

with open(ref_file, "r") as f:
    ref_text = f.read()

with open(can_file, "r") as f:
    can_text = f.read()
    
ref_text = ref_text.split("\n")
can_text = can_text.split("\n")
    
s = 0
ref_corpus = []
can_corpus = []

for ref, can in zip(ref_text, can_text):
    ref_corpus.append([ref.split(" ")])
    can_corpus.append(can.split(" "))

print(corpus_bleu(ref_corpus, can_corpus))
