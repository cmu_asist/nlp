import numpy as np
import pandas as pd
from nltk.corpus import wordnet
import nltk  
from nltk.stem import WordNetLemmatizer
from nltk.corpus import stopwords
import re
import operator
from collections import Counter



# from sklearn.metrics import confusion_matrix
# from sklearn.metrics import precision_recall_fscore_support
# from sklearn.feature_extraction.text import TfidfVectorizer

#code use
# import level2_code
# sentence=["I will switch to the searcher instead of heavy."]
# level2_code.predict(sentence)
# Output: [['role']]

feature_dict={}
feature_dict={}
feature_dict['tools']=['tool','hammer', 'stretcher', 'durability','replenish','equipment','break','utility','kit','weapon']
feature_dict['role']=['role','medic','specialist','searcher','switch','engineer']
feature_dict['rubble']=['gravel', 'rubble','clear','pile']
feature_dict['markers']=['marker', 'mark','label','put','number','unmark']
feature_dict['victim']=['victim','critical','person','patient','find','save','revive','rescue']

lemmatizer = WordNetLemmatizer()

def get_wordnet_pos(word):
    """Map POS tag to first character lemmatize() accepts"""
    tag = nltk.pos_tag([word])[0][1][0].upper()
    tag_dict = {"J": wordnet.ADJ,
                "N": wordnet.NOUN,
                "V": wordnet.VERB,
                "R": wordnet.ADV}

    return tag_dict.get(tag, wordnet.NOUN)

def preprocess_lemmetize(lemmatizer,sentence):
    wordsList=nltk.word_tokenize(sentence)
    tagged = nltk.pos_tag(wordsList)
    tags = [t[1][0] for t in tagged]

    tag_dict = {"J": wordnet.ADJ,
                    "N": wordnet.NOUN,
                    "V": wordnet.VERB,
                    "R": wordnet.ADV}
    tags_wn=[tag_dict.get(tag, wordnet.NOUN) for tag in tags]
    lem_sentence=" ".join([lemmatizer.lemmatize(w,t) for w,t in zip(wordsList,tags_wn)])
    #lem_sentence=" ".join([lemmatizer.lemmatize(w, get_wordnet_pos(w)) for w in nltk.word_tokenize(sentence)])
    return lem_sentence

def preprocess_stopwords(sentence):
    stop_sentence=" ".join([word for word in  nltk.word_tokenize(sentence) if word not in stopwords.words('english')])
    return stop_sentence

def preprocess_spechar(sentence):
    return re.sub(r"[^a-zA-Z0-9]+", ' ', sentence)

def feature_label(feature,text):
    text=text.lower()
    for f in feature :
        if f in text.split():
        #if f in text:
            return True
    return False

def rulebased_lowlevel(transcript,feature_dict):
#     victim_features=["victim","one","two","three","yellow","green"]
#     rubble_features=["rubble","blocked"]
    final_label=[]
    for trans in transcript:
    #for l, t in zip(high_labels,transcript):
        trans=preprocess_stopwords(trans)
        trans=preprocess_spechar(trans)
        trans=preprocess_lemmetize(lemmatizer,trans)
        
        
        lab=[]
        for label, feat in feature_dict.items():
            
            if feature_label(feat,trans):
                lab.append(label)
                
        if len(lab)==0:
            lab.append('no_label') 
        
        final_label.append(lab) 
        
            
    return final_label


def rulebased_marker(transcript,prediction):
#     victim_features=["victim","one","two","three","yellow","green"]
#     rubble_features=["rubble","blocked"]
    feature_dict={'markers':['ones','twos','one','two']}
    not_allowed =['victim','markers']
    
    
    final_prediction=[]
    
    for trans,pred in zip(transcript,prediction):
    #for l, t in zip(high_labels,transcript):
        if len(set(not_allowed) & set(pred)) > 0:
            final_prediction.append(pred)
            continue
            
        trans=preprocess_stopwords(trans)
        trans=preprocess_spechar(trans)
        trans=preprocess_lemmetize(lemmatizer,trans)
        
        for label, feat in feature_dict.items():
            
            if feature_label(feat,trans):
                #print("pred 0", pred,trans)
                if 'no_label' in pred:
                    pred = [label]
                else:
                    pred.append(label)
                #print("pred 1", pred)
                
        final_prediction.append(pred)
            
    return final_prediction

def predict(sentence):
    
    predicted_labels=rulebased_lowlevel(sentence,feature_dict)
    return predicted_labels

def predict_improved_markers(sentence):
    
    predicted_labels=rulebased_lowlevel(sentence,feature_dict)
    predicted_labels_new= rulebased_marker(sentence,predicted_labels)
    return predicted_labels
